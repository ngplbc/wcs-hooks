import React, { useState } from "react";
import './App.css';

function App() {
  return (
    <div className="App">
      <Counter />
    </div>
  );
}

const Counter = () => { 
  const[count, setCount] = useState(0);

  return (
    <div>
      <p>The counter is at : {count} </p>
      <button onClick={() => setCount(count + 1)}>>
        +1
      </button>
      <button onClick={() => setCount(count - 1)}>>
        -1
      </button>
      <input type="number" onChange={(e) => setCount(Number(e.target.value))}/>
    </div>
  );
}

export default App;
